#!/usr/bin/env python

import sys
import os
import io
import json
import uuid
import plyvel

def write_outfile(emp_list, filename, source):
    try:
        count = 0
        fo = io.open(filename, "a", encoding="utf8")
        if fo.tell() == 0:
            fo.write(u"Id;Employee;Role;Email Address;Generic Phone;Direct Phone;City;State;Source\n")
        else:
            count = sum(1 for line in open(filename)) - 1
        for r in emp_list:
            fo.write("%s;%s;%s;%s;%s;%s;%s;%s;%s;%s" % (str(count),r['Employee'],r['Role'],r['Email Address'],r['Generic Phone'],r['Direct Phone'],r['City'],r['State'],source,"\n"))
            count += 1
        fo.close()
        print "[+] %d result(s) written to %s" % (len(emp_list), filename)
    except IOError:
        print "[-] File error, printing results..."

def write_dbfile(emp_list, filename, source):
    try:
        db = plyvel.DB(filename, create_if_missing=True)
        records_db = db.prefixed_db(b'emp-')
        for record in emp_list:
            record['Source'] = source
            eid = uuid.uuid3(uuid.NAMESPACE_DNS, record['Employee'].encode('utf-8'))
            value = records_db.get(str(eid)) #check if record exists
            try:
                if json.loads(value)['Source'] != 'business-lists.com': #don't overwrite any business-lists records
                    records_db.put(str(eid), json.dumps(record, ensure_ascii=True))
            except TypeError: #TypeError occurs if record doesn't exist yet
                records_db.put(str(eid), json.dumps(record, ensure_ascii=True))
        print "[+] %d result(s) written to %s" % (len(emp_list), filename)
    except IOError:
        print "[-] File error, printing results..."
